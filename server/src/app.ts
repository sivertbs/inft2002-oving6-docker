import express from 'express';
import appRouter from './app-router';

/**
 * Express application.
 */
const app = express();

app.use(express.json());

// Since API is not compatible with v1, API version is increased to v2
app.use('/api/v2', appRouter);

export default app;
