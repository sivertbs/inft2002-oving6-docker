import pool from './mysql-pool';
import type { RowDataPacket, ResultSetHeader } from 'mysql2';

export type App = {
  text: string;
  stdOutput: string;
  stdError: string;
  exitStatus: number;
};

class AppService {
  /**
   * Stard docker container node:latest and give text input to bash terminal with -e argument.
   */
  runScript(text: string) {
    return new Promise<App | undefined>((resolve, reject) => {
      let results: App = { text: text, stdOutput: '', stdError: '', exitStatus: NaN };
      const { spawn } = require('node:child_process');
      const new_container = spawn('docker', ['run', '-ti', '--rm', 'node:latest', 'bash']);
      const bash_input = spawn('node', ['-e', text]);

      //@ts-ignore
      new_container.stdout.on('data', (data) => {
        bash_input.stdin.write(data);
      });

      //@ts-ignore
      new_container.stderr.on('data', (data) => {
        reject(data);
      });

      //@ts-ignore
      new_container.on('close', (code) => {
        bash_input.stdin.end();
      });

      //@ts-ignore
      bash_input.stdout.on('data', (data) => {
        results.stdOutput = data.toString();
      });

      //@ts-ignore
      bash_input.stderr.on('data', (data) => {
        results.stdError = data.toString();
      });

      //@ts-ignore
      bash_input.on('close', (code) => {
        results.exitStatus = code;
        resolve(results);
      });
    });
  }
}

const appService = new AppService();
export default appService;
