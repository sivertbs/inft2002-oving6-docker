import express from 'express';
import appService from './app-service';

/**
 * Express router containing app methods.
 */
const router = express.Router();

router.post('/app', (request, response) => {
  const data = request.body;
  //console.log(data);
  if (data && data.text && data.text.length != 0)
    appService
      .runScript(data.text)
      .then((res) => response.send(res))
      .catch((error) => response.status(500).send(error));
  else response.status(400).send('Missing javaScript text');
});

export default router;
