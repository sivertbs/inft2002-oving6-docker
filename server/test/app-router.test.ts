import axios from 'axios';
import pool from '../src/mysql-pool';
import app from '../src/app';
import appService, { App } from '../src/app-service';

const testApp: App = {
  text: "console.log('Hello');\nconsole.error('World');",
  stdOutput: 'Hello',
  stdError: 'World',
  exitStatus: 0,
};

// Since API is not compatible with v1, API version is increased to v2
axios.defaults.baseURL = 'http://localhost:3001/api/v2';

jest.mock('../src/app-service');

let webServer: any;
beforeAll((done) => {
  // Use separate port for testing
  webServer = app.listen(3001, () => done());
});

// Stop web server and close connection to MySQL server
afterAll((done) => {
  if (!webServer) return done(new Error());
  webServer.close(() => pool.end(() => done()));
});

describe('Run script through docker container (POST)', () => {
  test('Run valid script (200 OK and Exit status 0)', (done) => {
    appService.runScript = jest.fn(() => Promise.resolve(testApp));

    axios.post('/app', { text: testApp.text }).then((response) => {
      let resaultApp: App = response.data;
      expect(response.status).toEqual(200);
      expect(resaultApp.stdOutput).toEqual('Hello');
      expect(resaultApp.stdError).toEqual('World');
      expect(resaultApp.exitStatus).toEqual(0);
      done();
    });
  });
});
