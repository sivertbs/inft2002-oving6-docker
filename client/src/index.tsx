import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route } from 'react-router-dom';
import { NavBar, Card, Alert, Form, Button } from './widgets';
import { AppJS } from './app-components';

ReactDOM.render(
  <HashRouter>
    <div>
      <Alert />
      <AppJS />
    </div>
  </HashRouter>,
  document.getElementById('root')
);
