import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type App = {
  text: string;
  stdOutput: string;
  stdError: string;
  exitStatus: number;
};

class AppService {
  /**
   * Get resault from text (javaScript) input.
   */
  runScript(text: string) {
    return axios //@ts-ignore
      .post<App>('/app', { text })
      .then((response) => response.data);
  }
}

const appService = new AppService();
export default appService;
