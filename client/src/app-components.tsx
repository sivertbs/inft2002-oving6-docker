import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column, Form, Button } from './widgets';
import { NavLink } from 'react-router-dom';
import appService, { App } from './app-service';
import { createHashHistory } from 'history';

const history = createHashHistory(); // Use history.push(...) to programmatically change path

export class AppJS extends Component {
  src_App: App = {
    text: "console.log('Hello'); \n\nconsole.error('World');",
    stdOutput: '',
    stdError: '',
    exitStatus: 0,
  };

  render() {
    return (
      <>
        <Card title="App.js">
          <Form.Textarea
            value={this.src_App.text}
            onChange={(event) => {
              this.src_App.text = event.currentTarget.value;
            }}
          ></Form.Textarea>
          <Button.Success
            onClick={() => {
              appService
                .runScript(this.src_App.text)
                .then((res) => (this.src_App = res))
                .catch((error) => Alert.danger(error.message));
            }}
          >
            Run
          </Button.Success>
        </Card>
        <Card title="Standard output">
          <div id="stdout">{this.src_App.stdOutput}</div>
        </Card>
        <Card title="Standard error">
          <div id="stderror">{this.src_App.stdError}</div>
        </Card>
        <Card title="Exit status: ">
          <div id="exitStatus">{this.src_App.exitStatus}</div>
        </Card>
      </>
    );
  }
}
