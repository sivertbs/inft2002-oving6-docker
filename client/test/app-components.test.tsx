import * as React from 'react';
import { AppJS } from '../src/app-components';
import { shallow } from 'enzyme';
import { Form, Button } from '../src/widgets';
import { NavLink } from 'react-router-dom';

jest.mock('../src/app-service', () => {
  class AppService {
    runScript(text: string) {
      return Promise.resolve({
        text: text,
        stdOutput: 'Hello - test',
        stdError: 'World - test',
        exitStatus: 0,
      });
    }
  }
  return new AppService();
});

describe('App component tests', () => {
  test('AppJS draws correctly', (done) => {
    const wrapper = shallow(<AppJS />);

    wrapper.find(Form.Textarea).simulate('change', {
      currentTarget: { value: "console.log('Hello - test');console.error('World - test')" },
    });
    // @ts-ignore
    expect(
      wrapper.containsMatchingElement(
        // @ts-ignore
        <Form.Textarea value="console.log('Hello - test');console.error('World - test')" />
      )
    ).toEqual(true);

    wrapper.find(Button.Success).simulate('click');
    setTimeout(() => {
      expect(wrapper.containsMatchingElement(<div id="stdout">{'Hello - test'}</div>)).toEqual(
        true
      );
      expect(wrapper.containsMatchingElement(<div id="stderror">{'World - test'}</div>)).toEqual(
        true
      );
      expect(wrapper.containsMatchingElement(<div id="exitStatus">{0}</div>)).toEqual(true);
      done();
    });
  });
});
